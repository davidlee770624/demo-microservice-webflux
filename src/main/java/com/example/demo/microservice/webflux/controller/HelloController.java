package com.example.demo.microservice.webflux.controller;

import com.example.demo.microservice.webflux.entity.Entity;
import com.example.demo.microservice.webflux.handler.EntityHandler;
import com.example.demo.microservice.webflux.repository.EntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@RestController
@Slf4j
public class HelloController {

    @Autowired
    private EntityHandler entityHandler;

    @Autowired
    private EntityRepository entityRepository;

    @GetMapping("/mono/{name}")
    public Mono<String> helloMono(@PathVariable("name") String name) {
        return Mono.just("hello Mono " + name);
    }

    /**
     * 使用flux，用Stream返回0-N個元素
     */
    @GetMapping(value = "/flux", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> flux() {
//        long timeMillis = System.currentTimeMillis();
//        log.info("webflux() start");
        return Flux.fromStream(IntStream.range(1, 5).mapToObj(I -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "flux data—" + I;
        }));
//        log.info("flux() end use time {}/ms", System.currentTimeMillis() - timeMillis);
//        return result;
    }

    @GetMapping(value = "/fill", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Entity> fill() {
        Entity entity1 = new Entity();
        entity1.setName("david");
        entityRepository.save(entity1);

        Entity entity2 = new Entity();
        entity2.setName("Johnny");
        entityRepository.save(entity2);

        return entityHandler.findAll();
    }

    @GetMapping(value = "/{id}")
    public Mono<Entity> findById(@PathVariable("id") Long id) {
        return entityHandler.findById(id);
    }

    @GetMapping()
    public Flux<Entity> findAll() {
        return entityHandler.findAll();
    }

    @PostMapping()
    public Mono<Long> save(@RequestBody Entity entity) {
        return entityHandler.save(entity);
    }

    @PutMapping()
    public Mono<Long> modify(@RequestBody Entity entity) {
        return entityHandler.modify(entity);
    }

    @DeleteMapping(value = "/{id}")
    public Mono<Long> delete(@PathVariable("id") Long id) {
        return entityHandler.delete(id);
    }


    @GetMapping("/randomNumbers")
    public Flux<ServerSentEvent<Integer>> randomNumbers() {
        return Flux.interval(Duration.ofSeconds(1))
                        .map(duration -> Tuples.of(duration, ThreadLocalRandom.current().nextInt()))
                        .map(data -> ServerSentEvent.<Integer>builder()
                                .event("TEST RANDOM")
                                .id(Long.toString(data.getT1()))
                                .data(data.getT2())
                                .build());
    }
}
