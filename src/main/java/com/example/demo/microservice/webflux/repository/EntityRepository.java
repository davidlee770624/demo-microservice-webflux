package com.example.demo.microservice.webflux.repository;

import com.example.demo.microservice.webflux.entity.Entity;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

// =DAO
@Repository
public class EntityRepository {

    private ConcurrentMap<Long, Entity> dataBase = new ConcurrentHashMap<>();
    private static AtomicLong idGenerator = new AtomicLong(0);

    public Long save(Entity entity) {
        Long id = idGenerator.incrementAndGet();
        entity.setId(id);
        dataBase.put(id, entity);
        return entity.getId();
    }

    public Collection<Entity> findAll() {
        return dataBase.values();
    }

    public Entity findById(Long id) {
        return dataBase.get(id);
    }

    public Long update(Entity entity) {
        dataBase.put(entity.getId(), entity);
        return entity.getId();
    }

    public Long delete(Long id) {
        dataBase.remove(id);
        return id;
    }
}
