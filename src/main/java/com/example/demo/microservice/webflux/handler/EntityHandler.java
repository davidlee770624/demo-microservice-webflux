package com.example.demo.microservice.webflux.handler;

import com.example.demo.microservice.webflux.entity.Entity;
import com.example.demo.microservice.webflux.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

@Component
public class EntityHandler {

    private  EntityRepository entityRepository;

    @Autowired
    public EntityHandler(EntityRepository repository) {
        this.entityRepository = repository;
    }

    public Mono<Long> save(Entity entity) {
        return Mono.create(monoSink -> monoSink.success(entityRepository.save(entity)));
    }

    public Mono<Entity> findById(Long id) {
        return Mono.justOrEmpty(entityRepository.findById(id));
    }

    public Flux<Entity> findAll() {
        Collection<Entity> data = entityRepository.findAll();
        return Flux.fromStream(data.stream().map(I -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return I;
        }));


//        return Flux.fromIterable(entityRepository.findAll());
    }

    public Mono<Long> modify(Entity entity) {
        return Mono.create(monoSink -> monoSink.success(entityRepository.update(entity)));
    }

    public Mono<Long> delete(Long id) {
        return Mono.create(MonoSink -> MonoSink.success(entityRepository.delete(id)));
    }
}
