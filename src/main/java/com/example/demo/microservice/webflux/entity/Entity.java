package com.example.demo.microservice.webflux.entity;

import lombok.Data;

@Data
public class Entity {
    private Long id;
    private String name;
}
